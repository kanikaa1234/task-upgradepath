Upgradepath
===========

An upgrade path check.

Upgradepath is a constraint that ensures the ability to upgrade from distro
release N to distro release N+1. In other words no package dependencies may
break when users want to upgrade their distro. That is achieved by requiring
the higher distro release to contain at least the same or higher package build
versions (in NEVR sense) than the lower distro release.

More information is at the moment available at
<https://fedoraproject.org/wiki/AutoQA_tests/Upgradepath>.

This check can be run with [Taskotron] framework.

[Taskotron]: https://fedoraproject.org/wiki/Taskotron


Execution
---------

Run the check standalone:

    $ ./upgradepath.py f20-updates

(see `--help` for full usage description)

or through the Taskotron runner, if you give it a valid non-testing koji tag:

    $ runtask -i f20-updates -t koji_tag runtask.yml


Contact
-------

This tool is developed as part of [Taskotron] framework. The same contacts and
the same ways to report a problem can be used.

