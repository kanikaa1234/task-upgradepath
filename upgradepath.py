#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2010-2014, Red Hat, Inc.
# License: GNU General Public License version 2 or later

''' An upgrade path check.

Upgradepath is a constraint that ensures the ability to upgrade from distro
release N to distro release N+1. In other words no package dependencies may
break when users want to upgrade their distro. That is achieved by requiring
the higher distro release to contain at least the same or higher package build
versions (in NEVR sense) than the lower distro release.
'''

import logging
if __name__ == '__main__':
    # Set up logging ASAP to see potential problems during import.
    # Don't set it up when not running as the main script, someone else handles
    # that then.
    logging.basicConfig()

import operator
import sys
import argparse
import os

from libtaskotron import check
from libtaskotron.ext.fedora import koji_utils
from libtaskotron.ext.fedora import bodhi_utils
from libtaskotron.ext.fedora import yumrepoinfo
from libtaskotron.ext.fedora import rpm_utils


logger = logging.getLogger('upgradepath')
logger.setLevel(logging.INFO)
logger.addHandler(logging.NullHandler())
args = None # argparse.Namespace object populated when cmdline args are parsed


class Upgradepath(object):

    # output right padding when printing out package details
    pkg_pad = 24


    def __init__(self):
        # mapping of package build -> CheckDetail
        self.build2detail = {}

        # mapping of BodhiUpdate -> CheckDetail
        self.update2detail = {}

        # dict of problematic builds (Koji/Bodhi problems): {NEVR: error}
        self.problematic = {}

        self.koji = koji_utils.KojiClient()
        #FIXME: self.koji.check_connection()

        self.repoinfo = yumrepoinfo.get_yumrepoinfo()
        self.bodhi = bodhi_utils.BodhiUtils()


    def store_logs(self):
        '''Take ``self.build2detail`` and ``self.update2detail`` and save it as
        inidividual log files in ``args.storedir`` directory, provided it is
        defined.
        Also sets ``artifact`` attribute for the affected details, to point to
        the generated artifact file.
        '''
        if not args.storedir:
            logger.debug('Not writing individual logs, not requested')
            return

        try:
            storedir = os.path.abspath(args.storedir)
            logger.debug('Writing individual logs into: %s', storedir)

            if not os.path.exists(storedir):
                os.makedirs(storedir)

            # save build logs
            for detail in self.build2detail.values():
                filepath = os.path.join(storedir, detail.item) + '.log'
                fout = open(filepath, 'w')
                fout.write('\n'.join(detail.output) + '\n')
                fout.close()
                detail.artifact = filepath

            # save update logs
            for update, detail in self.update2detail.items():
                filename = update_id(update.update) + '.log'
                # update titles can be very long, we need to trim them
                if len(filename) > 255:  # the max file name length for most Linux filesystems
                    suffix = '....log'
                    filename = filename[:255-len(suffix)] + suffix
                    assert len(filename) <= 255
                filepath = os.path.join(storedir, filename)
                fout = open(filepath, 'w')
                fout.write('\n'.join(detail.output) + '\n')
                fout.close()
                detail.artifact = filepath

        except OSError as e:
            logger.error('Individual logs not saved into %s, an error occured. '
                         'Raising.', args.storedir)
            raise e


    def compare(self, proposed_build, tags, op, tag_builds, check_detail,
                check_pending=False):
        '''Compare proposed_build with given operator to latest build in
        all tags and update check_detail.outcome.
        @param proposed_build Koji Build object
        @param tags list of kojitags or list of tuples containing kojitags
                    which should be checked for upgradepath conformance. If
                    tuples are provided, then the contents of corresponding
                    repos will be united while searching for latest build.
                    Example: ['f15', 'f16-updates'] or
                             [('f15',), ('f16', 'f16-updates')]
        @param op comparison operator from @module operator
        @param tag_builds dictionary that maps every available tag to the latest
                          Koji build object (or None) available for this tag
        @param check_detail CheckDetail object related to the proposed build
        @param check_pending look also into Bodhi for pending requests into
                             stable and consider it in the algorithm
        @return overall check result of this comparison ('PASSED', 'INFO' or
                'FAILED')
        '''
        # convert 'op' to human language for error message clarity
        opstr = {operator.eq: ['==', 'equal to'],
                 operator.ge: ['>=', 'greater than or equal to'],
                 operator.gt: ['>', 'greater than'],
                 operator.le: ['<=', 'lesser than or equal to'],
                 operator.lt: ['<', 'lesser than'],
                 operator.ne: ['!=', 'not equal to']}
        assert op in opstr, 'Used unsupported operator: %s' % op

        infos = 0
        fails = 0

        # check the build against all proposed tags or set of tags
        for tag_tuple in tags:
            # convert everything to tuples
            if isinstance(tag_tuple, basestring):
                tag_tuple = (tag_tuple,)

            # make human readable string for this tag tuple,
            # e.g. 'f22 + f22-updates'
            tag_str = ' + '.join([tag for tag in tag_tuple])

            # find the latest build in repositories
            latest_main_build = None
            for tag in tag_tuple:
                build = tag_builds[tag]
                if cmpKojiNEVR(build, latest_main_build) > 0:
                    latest_main_build = build

            # find the latest build in proposed updates
            if check_pending:
                latest_pend_build = self.check_pending_request(
                    proposed_build['name'], tag_tuple)

            # find the completely latest build
            latest_build = latest_main_build
            if (check_pending and
                cmpKojiNEVR(latest_pend_build, latest_build) > 0):
                latest_build = latest_pend_build

            # compare and find out the result
            if latest_build:
                res = cmpKojiNEVR(proposed_build, latest_build)
                ok = op(res, 0)
            else:
                ok = True

            if latest_main_build:
                res = cmpKojiNEVR(proposed_build, latest_main_build)
                ok_main = op(res, 0)
            else:
                ok_main = True

            # we need to know whether pending build 'saved' the result
            # (changed it from fail to pass)
            pend_needed = (ok and not ok_main)

            # print the results
            if pend_needed:
                keyword = '[INFO]'
                infos += 1
            elif ok:
                keyword = '[ OK ]'
            else:
                keyword = '[FAIL]'
                fails += 1

            check_detail.store('{0:<7}{1}'.format(keyword, tag_str))
            check_detail.store(' '*8 +
                     'Latest package: '.ljust(Upgradepath.pkg_pad) +
                     (koji_utils.getNEVR(latest_main_build) if latest_main_build
                     else 'None'))
            if check_pending:
                check_detail.store(' '*8 +
                         'Latest pending package: '.ljust(Upgradepath.pkg_pad) +
                         (koji_utils.getNEVR(latest_pend_build)
                         if latest_pend_build else 'None'))

            if not ok:
                check_detail.store(' '*8 + 'Error: Condition not satisfied: '
                    '%s %s %s' % (koji_utils.getNEVR(proposed_build), opstr[op][0],
                                  koji_utils.getNEVR(latest_build)))
                check_detail.store(' '*8 + 'Error: Proposed package version must '
                    'be %s the latest (pending) package version.' % opstr[op][1])

            if pend_needed:
                check_detail.store(' '*8 + 'Info: The pending package must be '
                    'pushed together with (or before) the proposed package, '
                    'otherwise the upgrade path will be broken.')

        # update the CheckDetail result
        result = 'PASSED'
        ## FIXME: uncomment the two lines below once our bodhi_update_state.py
        ## learns about the INFO result
        #if infos > 0:
        #   result = 'INFO'
        if fails > 0:
            result = 'FAILED'
        check_detail.update_outcome(result) # FIXME: result -> outcome

        # return the final result
        return result


    def check_pending_request(self, pkgname, tag_tuple):
        '''Look into Bodhi for pending request into stable relevant to @param
        tag_tuple for the same package as @param proposed_build.
        @param pkgname package name (string)
        @param tag_tuple same as in @method self.compare()
        @return Koji build object of the latest pending request or None if there
                is no such request
        '''
        # get all top parents from the tags in tag_tuple
        top_parents = set()
        for tag in tag_tuple:
            reponame = self.repoinfo.repo_by_tag(tag)['name']
            top_parents.add(self.repoinfo.top_parent(reponame))

        # there should be exactly one top parent
        # (we usually check e.g. f20 or f20 + f20-updates, which
        # means only one top parent)
        assert len(top_parents) == 1, \
            "Multiple repo top parents found, that's weird: %s" % top_parents

        # get the top parent tag, it should e.g. 'f20'
        tag = top_parents.pop()

        # discard EOL releases and Rawhide, consider just stable and Branched
        if (tag not in self.repoinfo.releases() and
            tag != self.repoinfo.branched()):
            return None

        # bodhi uses names in format 'F19', 'F20', let's convert it
        bodhi_tag = tag.upper()

        # query bodhi for the last update of this package that just requests
        # going into main or stable updates repo
        requests = self.bodhi.client.query(packages=pkgname, request='stable',
                                           releases=[bodhi_tag], limit=1)

        # if there is no Bodhi request, there is nothing to do
        if not requests['updates']:
            return None

        # we found our update. this first one should be the most recent one
        # (yuck, not optimal! but we use that approach everywhere anyway)
        update = requests['updates'][0]

        # this update may contain several builds (different packages). we need
        # to find that one that contains our package name
        builds = [build for build in update['builds']
                  if rpm_utils.rpmformat(build['nvr'], 'n') == pkgname]
        # there should be exactly one such build
        assert len(builds) == 1, ('Bodhi update should not contain multiple '
            'builds of the same package: %s' % builds)
        build = builds[0]

        # now we need to query Koji to get all relevant information about this
        # build (like epoch)
        koji_build = self.koji.session.getBuild(build['nvr'])
        assert koji_build is not None, 'No such package in Koji: %s' % build['nvr']

        return koji_build


    def run_once(self, kojitag, event='post-bodhi-update-batch', **kwargs):
        # Ideally upgradepath should check just the new updates. But since we don't
        # yet support check re-scheduling [1], we have to work around somehow [2].
        # So let's just run upgradepath for *all* updates requesting their move to
        # <kojitag>. It is a little inefficient, but if for some package the result
        # changes, we will report it correctly (and that's more important).
        # When check re-scheduling is supported, we can revert this check from whole
        # tag checking to just new updates checking.
        # [1] https://fedorahosted.org/autoqa/ticket/245
        # [2] https://fedorahosted.org/autoqa/ticket/246

        # THE UPGRADEPATH ALGORITHM:
        #
        # == Pushing to main repository ==
        # Pushing PKG to F(N)-main means:
        #   1. PKG in F(lower)-main <= PKG to push
        #   2. PKG in F(higher)-main >= PKG to push
        #
        # == Pushing to updates repository ==
        # Pushing PKG to F(N)-updates means:
        #   1. PKG in F(lower)-main <= PKG to push
        #   2. PKG in F(lower)-updates <= PKG to push
        #   3. PKG in F(higher)-main union F(higher)-updates
        #      union F(higher)-updates-pending => PKG to push
        #
        # Note 1: Originally the last step didn't use -pending repository. But
        #         then we found out it caused many failures reported to package
        #         maintainers (when they pushed an update to several releases
        #         at once) although they didn't have any means to improve that
        #         condition. As long as the main audience is package maintainers
        #         (instead of RelEng), we want to consider -pending repo as well.
        #
        # Note 2: If PKG doesn't exist in REPO, it also satisfies any condition.
        #
        # References:
        #   https://fedorahosted.org/autoqa/ticket/309
        #   https://fedorahosted.org/autoqa/ticket/277
        #   https://fedorahosted.org/autoqa/ticket/330
        #
        # FIXME: Branched releases before "Final Change Deadline" use -updates
        # tag for pushing into -main repository. That breaks this algorithm.
        # See: https://fedorahosted.org/autoqa/ticket/277#comment:2
        # For proper functionality we need to detect such branched releases and
        # adjust the algorithm accordingly (run "Pushing to main repository" part
        # even if the tag does not correspond to it).

        # Get a list of all repos we monitor; currently not -testing, see:
        # https://lists.fedorahosted.org/pipermail/autoqa-devel/2010-September/001189.html
        # https://fedorahosted.org/autoqa/ticket/231
        repos = [self.repoinfo.repo(reponame) for reponame in
                 self.repoinfo.repos() if not reponame.endswith('-testing') and
                 self.repoinfo.release_status(reponame) != 'obsolete']
        assert kojitag in [repo['tag'] for repo in repos], \
            'Requested unsupported kojitag: %s' % kojitag

        # are we pushing to main repo or to updates repo?
        # we recognize main repository when it has no parent
        push_to_main = not bool(self.repoinfo.repo_by_tag(kojitag)['parent'])

        if push_to_main:
            # we need to get list of main repositories for F(higher) and F(lower)
            low_repos = [repo for repo in repos if not repo['parent']
                                                   and repo['tag'] < kojitag]
            hi_repos = [repo for repo in repos if not repo['parent']
                                                  and repo['tag'] > kojitag]
        else: # pushing to update repo
            # for both F(lower) and F(higher) we are interested in main and
            # update repos
            low_repos = [repo for repo in repos if repo['tag'] < kojitag]
            hi_repos = [repo for repo in repos if repo['tag'] > kojitag]
        reposorter = lambda repo : repo['name']
        low_repos = sorted(low_repos, key=reposorter)
        hi_repos = sorted(hi_repos, key=reposorter)

        # gather list of builds to check
        if event == "post-bodhi-update-batch":
            # get the list of all builds requesting move to the kojitag
            sourcetag = kojitag + '-pending'
            logger.debug('Koji tag to be checked: %s' % sourcetag)
            logger.info('Querying Koji for builds...')
            # FIXME: koji.session
            updates = self.koji.session.listTagged(sourcetag)
            nevrs = sorted([koji_utils.getNEVR(u) for u in updates])
        else: # event is post-bodhi-update
            nevrs = kwargs['nvrs']
        logger.debug('%s builds to be checked:\n  %s', len(nevrs),
                     '\n  '.join(nevrs))

        # For every build checked get a Bodhi update that corresponds to it.
        # Check that all builds for that update are scheduled for testing.
        # If everything is ok, put that into a dictionary:
        #
        # build2update = {build NEVR: BodhiUpdate}
        #
        # If there is some problem, put that build into self.problematic and
        # remove it from 'nevrs'.

        logger.info('Querying Bodhi for matching updates...')
        build2update, failures = self.bodhi.build2update(nevrs, strict=True)

        for build, update in failures.items():
            if update is None:
                # build not found in Bodhi
                msg = 'Update containing %s not found in Bodhi' % build
            else:
                # some other builds from the matching update were not scheduled
                msg = ("Build %s is part of an incompletely scheduled Bodhi "
                       "update '%s'." % (build, update_id(update)))
            logger.warning(msg)
            # FIXME: find out how to report these errors, currently they are not
            # easily visible anywhere (ResultsDB or elsewhere)
            self.problematic[build] = msg
            nevrs.remove(build)

        # we need Bodhi object to be hashable to use them in sets and dicts
        # let's convert it
        bodhi_objects = set()
        for build, update in build2update.items():
            same = [bo for bo in bodhi_objects if bo.update is update]
            if same:
                build2update[build] = same[0]
            else:
                build2update[build] = BodhiUpdate(update)
                bodhi_objects.add(build2update[build])

        # print some debugging Bodhi info
        def bodhi_desc(update):
            return '%s (%s)' % (update_id(update.update), update.update['title'])
        logger.debug('%s Bodhi updates found:\n  %s',
                     len(set(build2update.values())),
                     '\n  '.join(sorted([bodhi_desc(update) for update in
                                         set(build2update.values())])))

        # Get build info from Koji for every NEVR
        # Use multicall to speed it up
        logger.info('Getting build information from Koji...')
        self.koji.session.multicall = True
        for nevr in nevrs:
            self.koji.session.getBuild(rpm_utils.rpmformat(nevr, 'nvr'))
        koji_builds = self.koji.session.multiCall()

        # Let the testing begin!
        logger.debug('Running main upgradepath checks...')
        for (nevr, koji_build) in zip(nevrs, koji_builds):
            logger.debug('Checking: %s', nevr)

            # Populate self.build2detail:
            # {build NEVR: CheckDetail}
            # Inside the CheckDetail use NVR and not NEVR, because we need to report NVR items
            # to ResultsDB.
            detail = check.CheckDetail(item=rpm_utils.rpmformat(nevr, fmt='nvr'),
                                       report_type=check.ReportType.KOJI_BUILD)
            self.build2detail[nevr] = detail

            detail.note = kojitag
            detail.store('%s\n%s into %s\n%s' % (60*'=', nevr, kojitag, 60*'='))

            # Koji returns list if the call succeeded and dict (containing error
            # information) if the call failed
            if isinstance(koji_build, dict):
                msg = "Can't fetch info about %s from Koji: %s" % (nevr, koji_build)
                detail.store(msg)
                self.problematic[nevr] = msg
                detail.outcome = 'ABORTED'
                continue
            proposed_build = koji_build[0]

            # if we want to push an older build, warn about that and perform further
            # checking for the most recent one
            # see https://fedorahosted.org/autoqa/ticket/230
            latest_builds = self.koji.session.listTagged(kojitag,
                                package=proposed_build['name'], latest=True)
            latest_build = latest_builds[0] if latest_builds else None
            if latest_build is not None:
                if cmpKojiNEVR(proposed_build, latest_build) <= 0:
                    msg = ('Warning: Same or newer build already exists in the '
                           'same repo.\nAll checks will be performed for: %s' %
                           koji_utils.getNEVR(latest_build))
                    detail.store(msg)
                    proposed_build = latest_build

            # query Koji for build information for all packages using multicall
            all_tags = [repo['tag'] for repo in repos]
            self.koji.session.multicall = True
            for tag in all_tags:
                self.koji.session.listTagged(tag, package=proposed_build['name'],
                                     latest=True)
            values = self.koji.session.multiCall()
            tag_builds = {}

            for tag, value in zip(all_tags, values):
                # value is dict if call failed
                if isinstance(value, dict):
                    logger.warn('Error querying Koji for %s: %s' % (tag, value))
                    tag_builds[tag] = None
                else:
                    # value is now [[koji_build]] or [[]]
                    tag_builds[tag] = value[0][0] if value[0] else None


            ### start the main testing phase ###

            if push_to_main:
                # compare with lower tags, so version has to be greater or equal
                low_tags = [repo['tag'] for repo in low_repos]
                self.compare(proposed_build, low_tags, operator.ge,
                             tag_builds, detail)

                # print infobar about currently proposed build
                detail.store('{0:<7}{1}'.format('[--->]', kojitag))
                detail.store(' '*8 +
                           'Proposed package: '.ljust(Upgradepath.pkg_pad) +
                           koji_utils.getNEVR(proposed_build))

                # compare with higher tags, so version has to be lower or equal
                hi_tags = [repo['tag'] for repo in hi_repos]
                self.compare(proposed_build, hi_tags, operator.le,
                             tag_builds, detail)

            else: # pushing to update repository
                # compare with lower tags, so version has to be greater or equal
                low_tags = [repo['tag'] for repo in low_repos]
                self.compare(proposed_build, low_tags, operator.ge,
                             tag_builds, detail)

                # print infobar about currently proposed build
                detail.store('{0:<7}{1}'.format('[--->]', kojitag))
                detail.store(' '*8 +
                           'Proposed package: '.ljust(Upgradepath.pkg_pad) +
                           koji_utils.getNEVR(proposed_build))

                # compare with higher tags, so version has to be lower or equal

                # we have to group corresponding main and update repos
                # let's define dict parent -> [child1, child2]
                # e.g. 'dist-f14' -> ['dist-f14-updates', 'dist-f14-updates-candidate']
                tag_family = {}
                for repo in hi_repos:
                    # this presumes all repos are sorted by name/tag
                    if not bool(repo['parent']):
                        tag_family[repo['tag']] = []
                    else:
                        main_parent = self.repoinfo.repo(repo['parent'])
                        if main_parent['tag'] in tag_family:
                            tag_family[main_parent['tag']].append(repo['tag'])
                        else:
                            # it might happen that there is no main_parent for
                            # a repo. e.g. we want to push to f20 (so we don't
                            # check this tag), but we still want to check f20-updates
                            # in this case just add it as a key
                            tag_family[repo['tag']] = []

                hi_tags = []
                for family in sorted(tag_family):
                    tag_tuple = [family]
                    tag_tuple.extend(tag_family[family])
                    hi_tags.append(tag_tuple)

                # now do the comparison finally
                # this time we also want to check for pending Bodhi requests
                self.compare(proposed_build, hi_tags, operator.le,
                             tag_builds, detail, check_pending=True)

            detail.store('RESULT: %s' % detail.outcome)

        # print broken updates
        if self.problematic:
            logger.warning('Broken updates found:')
            for nevr, msg in self.problematic.items():
                logger.warning('%s: %s' % (nevr, msg))

        # print summary
        summary = check.CheckDetail.create_multi_item_summary(
                    self.build2detail.values())
        logger.info('OVERALL SUMMARY: ' + summary)

        # Aggregate individual build results to Bodhi update results
        for build, update in sorted(build2update.iteritems(), key=lambda t: t[0]):
            if update not in self.update2detail:
                self.update2detail[update] = check.CheckDetail(
                    item=update_id(update.update),
                    report_type=check.ReportType.BODHI_UPDATE)

            update_detail = self.update2detail[update]
            build_detail = self.build2detail[build]
            update_detail.update_outcome(build_detail.outcome)
            update_detail.output.extend(build_detail.output)

        for update, detail in self.update2detail.iteritems():
            builds = [b for b, u in build2update.iteritems() if u is update]
            bdetails = [d for b, d in self.build2detail.iteritems() if b in builds]
            summary = check.CheckDetail.create_multi_item_summary(bdetails)
            detail.note = '%s; %s' % (summary, kojitag)
            detail.store('\nSUMMARY: %s' % summary, printout=False)

        # save logs if requested
        self.store_logs()

        # report results
        build_details = sorted(self.build2detail.values(), key=lambda d: d.item)
        update_details = sorted(self.update2detail.values(), key=lambda d: d.item)
        details = build_details + update_details
        return check.export_YAML(details)


def update_id(update):
    '''Extract some kind of update identifier: alias or updateid or title.
    FIXME: This is a temporary hack until Bodhi1 updates disappear from Bodhi2 system:
    https://phab.qadevel.cloud.fedoraproject.org/T578
    @param update Bodhi update object (dict)
    '''
    return update.get('alias') or update.get('updateid') or update['title']


def cmpKojiNEVR(nevr1, nevr2):
    '''Compare two Koji objects using NEVR. Handle None values.
    @param nvr1 Koji build object or None
    @param nvr2 Koji build object or None
    @return -1/0/1 if nevr1 < nevr2 / nevr1 == nevr2 / nevr1 > nevr2
    '''
    # handle empty input
    if not nevr1:
        return 0 if not nevr2 else -1
    if not nevr2:
        return 1
    # compare
    return rpm_utils.cmpNEVR(koji_utils.getNEVR(nevr1),
                             koji_utils.getNEVR(nevr2))


class BodhiUpdate(object):
    '''This a custom object wrapper for Bodhi update object. The purpose of this
    class is to make Bodhi updates hashable (therefore objects, the original ones
    are dictionaries), so that we can create dictionaries with Bodhi updates as
    their keys.'''
    def __init__(self, update):
        self.update = update


def main(custom_args=None):
    '''Main program loop
    @param custom_args if provided, the parser uses this instead of sys.argv. A
                       list of strings. Example: ['--debug', 'f20-updates']
    '''
    global args
    args = parse_args(custom_args)
    upgradepath = Upgradepath()
    return upgradepath.run_once(args.koji_tag)


def parse_args(custom_args=None):
    '''Parse cmdline arguments.
    @param custom_args if provided, the parser uses this instead of sys.argv. A
                       list of strings. Example: ['--debug', 'f20-updates']
    @return parsed args (an argparse.Namespace object)
    '''
    parser = argparse.ArgumentParser(description='Check the upgrade path. Read '
        'more at https://fedoraproject.org/wiki/AutoQA_tests/Upgradepath')
    parser.add_argument('koji_tag', metavar='KOJI_TAG',
        help='The tag that you want the builds to check against. Example: '
        '"f20-updates"')
    # FIXME: implement --builds
#    parser.add_argument('--builds', nargs='+',
#        help='NEVR of the proposed build or builds. If not given, then all '
#        'builds in the -pending version of KOJI_TAG get checked.')
    parser.add_argument('--debug', action='store_true', help='print debugging '
        'info')
    parser.add_argument('--storedir', help='An output directory where to store '
        'individual per-build/update results. If not provided, they are not '
        'generated at all.')

    args = parser.parse_args(custom_args) # if custom_args==None, then default
                                          # sys.argv is used
    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.getLogger('libtaskotron').setLevel(logging.DEBUG)

    logger.debug('Cmdline args: %s', args)

    # validate args
    if '-testing' in args.koji_tag:
        parser.error('Koji tags ending in "-testing" are not supported at the '
                     'moment')

    # strip unnecessary '-pending' from koji tag, if provided
    if args.koji_tag.endswith('-pending'):
        args.koji_tag = args.koji_tag[:-len('-pending')]
        logger.info('It seems the source instead of the target Koji tag was '
                    'specified. Assuming this one instead: %s' % args.koji_tag)

    return args


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        logger.critical('Interrupted by user, exiting...')
        sys.exit(1)
